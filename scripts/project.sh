#!/bin/sh

_pj_log() {
    echo "[INFO ] $*"
}

read_config() {
    _pj_log Checking if "$1" is a readable file. 
    test -r "$1" || exit 10

    _pj_log Reading file.
    set -o allexport
    # shellcheck disable=SC1090
    . "$1"

    _pj_log Read the following properties:
    # _ep_log server_url: "${server_url}" server_protocol: "${server_protocol}" 

    [ -n "${server_url}" ] && export PDM_UPDATE_BOT_GITLAB_SERVER_ADDRESS="${server_url}"
    [ -n "${server_protocol}" ] && export PDM_UPDATE_BOT_GITLAB_SERVER_PROTOCOL="${server_protocol}"
    [ -n "${api_token}" ] && export PDM_UPDATE_BOT_GITLAB_APITOKEN="${api_token}"
    [ -n "${dry_run}" ] && export PDM_UPDATE_BOT_GITLAB_DRY_RUN="${dry_run}"
    [ -n "${notifiication}" ] && export PDM_UPDATE_BOT_NOTIFY="${notifiication}"
    [ -n "${name}" ] && export PDM_UPDATE_BOT_PROJECT_NAME="${name}"
    [ -n "${repo_path}" ] && export PDM_UPDATE_BOT_PROJECT_REPO_PATH="${repo_path}"
    [ -n "${target_branch}" ] && export PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH="${target_branch}"
    [ -n "${main_branch}" ] && export PDM_UPDATE_BOT_PROJECT_MAIN_BRANCH="${main_branch}"
    [ -n "${project_id}" ] && export PDM_UPDATE_BOT_PROJECT_GITLAB_PROJECT_ID="${project_id}"
}

exit_on_error() {
    echo "[ERROR] Environment URL $1 was not provided." >&2
    exit 11
}

check_config() {
    _pj_log Checking environment variables
    [ -z "${PDM_UPDATE_BOT_GITLAB_SERVER_ADDRESS}" ] && exit_on_error "PDM_UPDATE_BOT_GITLAB_SERVER_ADDRESS"
    [ -z "${PDM_UPDATE_BOT_GITLAB_SERVER_PROTOCOL}" ] && exit_on_error "PDM_UPDATE_BOT_GITLAB_SERVER_PROTOCOL"
    [ -z "${PDM_UPDATE_BOT_GITLAB_APITOKEN}" ] && exit_on_error "PDM_UPDATE_BOT_GITLAB_APITOKEN"
    [ -z "${PDM_UPDATE_BOT_PROJECT_NAME}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_NAME"
    [ -z "${PDM_UPDATE_BOT_PROJECT_REPO_PATH}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_REPO_PATH"
    [ -z "${PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH"
    [ -z "${PDM_UPDATE_BOT_PROJECT_MAIN_BRANCH}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_MAIN_BRANCH"
    [ -z "${PDM_UPDATE_BOT_PROJECT_GITLAB_PROJECT_ID}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_GITLAB_PROJECT_ID"
    _pj_log Check completed successfully
}

run() {
    _pj_log Starting project runner
    /opt/pdm-update-bot/project-run.sh

    _pj_log "Update done"
}

PATH=${HOME}/.local/bin:${PATH}

read_config "$1"

check_config

run

_pj_log Stopping project ...

exit 0
