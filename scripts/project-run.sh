#!/bin/sh -e

_pr_log() {
    echo "[INFO ] $*"
}

on_exit() {
    _pr_log "Exiting runner"
    cd /
    [ -n "${PDM_UPDATE_BOT_PROJECT_DIR}" ] && rm -rf "${PDM_UPDATE_BOT_PROJECT_DIR}"
}

exit_on_error() {
    echo "[ERROR] Environment URL $1 was not provided." >&2
    exit 11
}

check_config() {
    _pr_log Checking environment variables
    [ -z "${PDM_UPDATE_BOT_GITLAB_SERVER_ADDRESS}" ] && exit_on_error "PDM_UPDATE_BOT_GITLAB_SERVER_ADDRESS"
    [ -z "${PDM_UPDATE_BOT_GITLAB_SERVER_PROTOCOL}" ] && exit_on_error "PDM_UPDATE_BOT_GITLAB_SERVER_PROTOCOL"
    [ -z "${PDM_UPDATE_BOT_GITLAB_APITOKEN}" ] && exit_on_error "PDM_UPDATE_BOT_GITLAB_APITOKEN"
    [ -z "${PDM_UPDATE_BOT_PROJECT_NAME}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_NAME"
    [ -z "${PDM_UPDATE_BOT_PROJECT_REPO_PATH}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_REPO_PATH"
    [ -z "${PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH"
    [ -z "${PDM_UPDATE_BOT_PROJECT_MAIN_BRANCH}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_MAIN_BRANCH"
    [ -z "${PDM_UPDATE_BOT_PROJECT_GITLAB_PROJECT_ID}" ] && exit_on_error "PDM_UPDATE_BOT_PROJECT_GITLAB_PROJECT_ID"
    _pr_log Check passed successfully
}

check_config

_pr_log Creating working directory

PDM_UPDATE_BOT_PROJECT_DIR=$(mktemp -d "pdmub.XXXX.${PDM_UPDATE_BOT_PROJECT_NAME}" -p /tmp)

_pr_log Using "${PDM_UPDATE_BOT_PROJECT_DIR}" as working directory

cd "${PDM_UPDATE_BOT_PROJECT_DIR}"

_pr_log Performing clone

git clone "${PDM_UPDATE_BOT_GITLAB_SERVER_PROTOCOL}://oauth2:${PDM_UPDATE_BOT_GITLAB_APITOKEN}@${PDM_UPDATE_BOT_GITLAB_SERVER_ADDRESS}/${PDM_UPDATE_BOT_PROJECT_REPO_PATH}" .

_pr_log Checking whether target branch exists

_target_branch_exists=$(git ls-remote --heads origin "${PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH}" | wc -l)

_pr_log Checking out target branch "${PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH}"

git checkout -b "${PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH}"

_pdm_args=

if [ "${PDM_UPDATE_BOT_GITLAB_DRY_RUN}" = "1" ]
then
    _pr_log This will be a dry run performance
    _pdm_args=--outdated
fi

_pr_log Disabling PDM update check
pdm config check_update false

_pr_log Starting PDM package installation
pdm install -G:all

_pr_log Performing update
_update_text=$(pdm update ${_pdm_args} | grep -v "Resolving:" | grep -v 'Lock successful')

_updates_done=$(echo "${_update_text}" | wc -l)

if [ "${_updates_done}" = "0" ]
then
    _pr_log No items to process
    on_exit
    exit 0
fi

if [ "${PDM_UPDATE_BOT_GITLAB_DRY_RUN}" != "1" ]
then
    _pr_log Adding lock file
    git add pdm.lock

    _pr_log Performing commit
    git commit -m "dep: Performing automated update

${_update_text}"

    if [ "${_target_branch_exists}" = "0" ]
    then
        _pr_log Branch is new - preparing pull request
        
        _update_text_escaped=$(/usr/bin/printf %q "${_update_text}")

        _pr_log Pushing to remote branch
        echo git push --force-with-lease --set-upstream origin "${PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH}" \
               -o merge_request.create \
               -o merge_request.target="${PDM_UPDATE_BOT_PROJECT_MAIN_BRANCH}" \
               -o merge_request.merge_when_pipeline_succeeds \
               -o merge_request.remove_source_branch \
               -o merge_request.title="chore: Update PDM dependencies" \
               -o merge_request.description="${_update_text_escaped}"
    else
        _pr_log Pushing to remote branch
        echo git push --force-with-lease --set-upstream origin "${PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH}"
    fi
fi

_pr_log Checking notification for "${PDM_UPDATE_BOT_NOTIFY}" frontend.
_notify_script=/opt/pdm-update-bot/notify-"${PDM_UPDATE_BOT_NOTIFY}".sh

if [ -x "${_notify_script}" ]
then
    _pr_log Found script.
    _title="PDM Update bot: ${PDM_UPDATE_BOT_PROJECT_NAME}"
    if [ "${PDM_UPDATE_BOT_GITLAB_DRY_RUN}" = "1" ]
    then
        _title="[Dry run] PDM Update bot: ${PDM_UPDATE_BOT_PROJECT_NAME}"
    fi

    _message="${_update_text}"

    _pr_log Running script
    "${_notify_script}" "${_title}" "${_message}"
    _pr_log Notification completed
fi

_pr_log Done

on_exit

exit 0
