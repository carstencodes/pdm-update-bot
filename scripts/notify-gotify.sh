#!/bin/sh -e

echo "[INFO ] Running parameter and configuration check"

[ -z "${PDM_UPDATE_BOT_GOTIFY_URL}" ] && exit 1

[ -z "${PDM_UPDATE_BOT_GOTIFY_APP_TOKEN}" ] && exit 2

[ -z "${PDM_UPDATE_BOT_GOTIFY_PRIORITY}" ] && export PDM_UPDATE_BOT_GOTIFY_PRIORITY=6

[ -z "${1}" ] && exit 3

[ -z "${2}" ] && exit 4

echo "[INFO ] Check passed"

echo "[INFO ] Sending notification to ${PDM_UPDATE_BOT_GOTIFY_URL} using token **** with priority ${PDM_UPDATE_BOT_GOTIFY_PRIORITY}"

curl -f -S "https://${PDM_UPDATE_BOT_GOTIFY_URL}/message?token=${PDM_UPDATE_BOT_GOTIFY_APP_TOKEN}" \
        -F "title=${1}" \
        -F "message=${2}" \
        -F "priority=${PDM_UPDATE_BOT_GOTIFY_PRIORITY}"

echo "[INFO] Notification sent"

exit 0
