# PDM update bot

The Python Development Master update bot is a simple shell script that can be hosted in a docker container.

It can either serve in a scheduled mode or a one-shot mode.
