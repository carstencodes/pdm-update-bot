ARG GOCROND_VERSION=22.9.1

FROM webdevops/go-crond:${GOCROND_VERSION}-alpine

ARG PDM_VERSION=2.1.5
ARG GITLAB_USER_MAIL=pdm-update-bot@localhost
ARG GITLAB_USER_DISPLAY_NAME=Gitlab\ Service\ Bot
ARG UID=250
ARG GID=250

ENV PDM_UPDATE_BOT_CONFIG_FILE=/opt/etc/pdm-update-bot/config.rc \
    PDM_UPDATE_BOT_CONFIG_FILES_DIR=/opt/etc/pdm-update-bot/config.rc.d/ \
    PDM_UPDATE_BOT_GITLAB_SERVER_ADDRESS=gitlab.com \
    PDM_UPDATE_BOT_GITLAB_SERVER_PROTOCOL=https \
    PDM_UPDATE_BOT_GITLAB_APITOKEN= \
    PDM_UPDATE_BOT_GITLAB_DRY_RUN=0 \
    PDM_UPDATE_BOT_NOTIFY= \
    PDM_UPDATE_BOT_GOTIFY_URL= \
    PDM_UPDATE_BOT_GOTIFY_PRIORITY=6 \
    PDM_UPDATE_BOT_GOTIFY_APP_TOKEN= \
    PDM_UPDATE_BOT_PROJECT_NAME= \
    PDM_UPDATE_BOT_PROJECT_REPO_PATH= \
    PDM_UPDATE_BOT_PROJECT_TARGET_BRANCH=pdm_update \
    PDM_UPDATE_BOT_PROJECT_MAIN_BRANCH=main \
    PDM_UPDATE_BOT_PROJECT_GITLAB_PROJECT_ID= 

COPY scripts/*.sh /opt/pdm-update-bot/
#ADD default.rc /opt/etc/pdm-update-bot/config.rc

# hadolint ignore=DL3018,SC2016
RUN chmod a+x /opt/pdm-update-bot/*.sh \
    && mkdir -p /opt/etc/pdm-update-bot/config.rc.d/ \
    && addgroup -S pdm-bot -g ${GID} \
    && adduser -S pdm-bot -G pdm-bot -h /home/pdm-bot -u ${UID} \
    && apk add --no-cache alpine-sdk python3-dev libffi-dev openssl-dev cargo python3 py-pip coreutils curl git \
    && echo PATH='"${PATH:+$PATH:}${HOME}/.local/bin"' >> /etc/profile  

USER pdm-bot

RUN pip install --no-cache-dir --user --no-warn-script-location --break-system-packages pdm==${PDM_VERSION} \
    && git config --global user.email "${GITLAB_USER_MAIL}" \
    && git config --global user.name "${GITLAB_USER_DISPLAY_NAME}" \
    && touch /home/pdm-bot/crontab

WORKDIR /

ENTRYPOINT [ "/usr/local/bin/go-crond", "pdm-bot:/home/pdm-bot/crontab", "--allow-unprivileged" ]
